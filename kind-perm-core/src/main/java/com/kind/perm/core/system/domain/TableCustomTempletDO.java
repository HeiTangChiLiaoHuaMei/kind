package com.kind.perm.core.system.domain;

import java.io.Serializable;

public class TableCustomTempletDO implements Serializable {
	private static final long serialVersionUID = -7367921243210104161L;
	
	private Long id;
	
	/**
	 * 类型编号
	 */
	private Long type;
	
	/**
	 * 描述的中文名称
	 */
	private String name;
	
	/**
	 *  对应的javaBean的DO的名称
	 */
	private String javaBean;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJavaBean() {
		return javaBean;
	}

	public void setJavaBean(String javaBean) {
		this.javaBean = javaBean;
	}
	


}
