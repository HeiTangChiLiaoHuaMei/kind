package com.kind.perm.core.demo.service.impl;

import java.util.List;

import com.kind.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.demo.dao.CommunityDao;
import com.kind.perm.core.demo.domain.CommunityDO;
import com.kind.perm.core.demo.service.CommunityService;

/**
 * 
 * 小区管理业务处理实现类. <br/>
 *
 * @date:2016年12月12日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class CommunityServiceImpl implements CommunityService {

	@Autowired
	private CommunityDao communityDao;

	@Override
	public PageView<CommunityDO> selectPageList(PageQuery pageQuery) {
        pageQuery.setPageSize(pageQuery.getPageSize());
		List<CommunityDO> list = communityDao.page(pageQuery);
		int count = communityDao.count(pageQuery);
        pageQuery.setItems(count);
		return new PageView<>(pageQuery, list);
	}

	@Override
	public int save(CommunityDO entity) throws ServiceException {
		try {
           return communityDao.saveOrUpdate(entity);

        }catch (Exception e){
		    e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }

	}

	@Override
	public CommunityDO getById(Long id) {
		return communityDao.getById(id);
	}

    @Override
    public void remove(Long id) throws ServiceException{
        try {
            communityDao.remove(id);

        }catch (Exception e){
            throw new ServiceException(e.getMessage());
        }
    }

	@Override
	public List<CommunityDO> queryList(CommunityDO entity) {
		return communityDao.queryList(entity);
	}
}
