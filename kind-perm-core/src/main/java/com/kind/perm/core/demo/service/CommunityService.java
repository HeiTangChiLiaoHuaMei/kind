package com.kind.perm.core.demo.service;

import java.util.List;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.demo.domain.CommunityDO;

/**
 * 小区业务处理接口<br/>
 *
 * @Date: 2016年12月12日
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public interface CommunityService {

    /**
     * 分页查询
     * @param pageQuery
     * @return
     */
	PageView<CommunityDO> selectPageList(PageQuery pageQuery);

    /**
     * 保存数据
     * @param entity
     */
	int save(CommunityDO entity);

    /**
     * 获取数据对象
     * @param id
     * @return
     */
    CommunityDO getById(Long id);

    /**
     * 删除数据
     * @param id
     */
	void remove(Long id);
	
	/**
	 * 查询符合条件的所有数据
	 * @param entity
	 * @return
	 */
	List<CommunityDO> queryList(CommunityDO entity);
}
