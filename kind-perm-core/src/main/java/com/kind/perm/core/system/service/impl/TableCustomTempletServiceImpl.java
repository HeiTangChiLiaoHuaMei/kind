package com.kind.perm.core.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kind.common.exception.ServiceException;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.system.dao.TableCustomTempletDao;
import com.kind.perm.core.system.domain.TableCustomDO;
import com.kind.perm.core.system.domain.TableCustomTempletDO;
import com.kind.perm.core.system.service.TableCustomTempletService;

/**
 * 
 * 导出业务处理实现类. <br/>
 *
 * @date:2017年01月19日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class TableCustomTempletServiceImpl implements TableCustomTempletService {

	@Autowired
	private TableCustomTempletDao tableCustomTempletDao;


	@Override
	public PageView<TableCustomTempletDO> selectPageList(PageQuery pageQuery) {
		pageQuery.setPageSize(pageQuery.getPageSize());
		List<TableCustomTempletDO> list = tableCustomTempletDao.page(pageQuery);
		int count = tableCustomTempletDao.count(pageQuery);
        pageQuery.setItems(count);
		return new PageView<>(pageQuery, list);
	}

	@Override
	public int save(TableCustomTempletDO entity) {
		try {
	           return tableCustomTempletDao.saveOrUpdate(entity);
	        }catch (Exception e){
			    e.printStackTrace();
	            throw new ServiceException(e.getMessage());
	        }
	}

	@Override
	public TableCustomTempletDO getById(Long id) {
		return tableCustomTempletDao.getById(id);
	}

	@Override
	public void remove(Long id) {
		try {
			tableCustomTempletDao.remove(id);
        }catch (Exception e){
            throw new ServiceException(e.getMessage());
        }
		
	}

	@Override
	public List<TableCustomTempletDO> queryList(TableCustomTempletDO entity) {
		return tableCustomTempletDao.queryList(entity);
	}
	
	
}
