<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="${ctx}/static/layer/layer.js"></script>
<script type="text/javascript">

var layerObj;
function openLayerWait(){
	layerObj = layer.load(3, {shade: [0.3,'#000']}); //0代表加载的风格，支持0-2
}

function closeLayerWait(){
	layer.close(layerObj);
}

</script>
