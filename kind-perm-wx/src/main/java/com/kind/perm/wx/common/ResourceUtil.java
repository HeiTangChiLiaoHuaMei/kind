package com.kind.perm.wx.common;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ResourceUtil
{

  public static String FILE_PATH = "";
  
  public static String WX_appid = "";

  public static String WX_appsecret = "";
  

  static
  {
	  Properties properties = getFILE_PATH();
	  FILE_PATH = properties.getProperty("file_path");
	  WX_appid = properties.getProperty("wx_appid");
	  WX_appsecret = properties.getProperty("wx_appsecret");
	  
  }


  public  static final Properties getFILE_PATH()
  {
	  Properties properties=new Properties();
	  try {
		  properties.load(new ResourceUtil().getClass().getResourceAsStream("/conf.properties"));
      } catch (FileNotFoundException e) {
          e.printStackTrace();
      } catch (IOException e) {
          e.printStackTrace();
      }
    return properties;
  }


}