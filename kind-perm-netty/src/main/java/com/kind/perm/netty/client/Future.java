package com.kind.perm.netty.client;

import java.util.concurrent.TimeUnit;

/**
 * 
 * User: 李明
 * Date: 2016/3/9
 * Time: 11:01
 * To change this template use File | Settings | File Templates.
 */
public interface Future<T> {

    T await() throws Exception;

    T await(long amount, TimeUnit unit) throws Exception;

}
