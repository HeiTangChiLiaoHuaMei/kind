package com.kind.perm.netty.utils.pack;

import java.io.IOException;

/**
 * 
 * User: 李明
 * Date: 2016/1/28
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
public interface Marshallable {

    /**
     * 消息编码类
     *
     * @param pack
     */
    void marshal(Pack pack);

    /**
     * 消息编码类
     *
     * @param unpack
     */
    void unmarshal(Unpack unpack) throws IOException;
}